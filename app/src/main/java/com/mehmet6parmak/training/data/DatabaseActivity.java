package com.mehmet6parmak.training.data;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.mehmet6parmak.training.data.db.FeedReaderContract;
import com.mehmet6parmak.training.data.db.FeedReaderDbHelper;

public class DatabaseActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtTitle;
    EditText edtSubTitle;

    ListView list;

    FeedReaderDbHelper dbHelper = new FeedReaderDbHelper(this);
    FeedEntryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        edtTitle = (EditText) findViewById(R.id.edt_title);
        edtSubTitle = (EditText) findViewById(R.id.edt_subtitle);

        list = (ListView) findViewById(R.id.list);

        findViewById(R.id.btn_add).setOnClickListener(this);
        findViewById(R.id.btn_delete).setOnClickListener(this);
        findViewById(R.id.btn_update).setOnClickListener(this);

        adapter = new FeedEntryAdapter(this, dbHelper.getFeedEntriesCursor(), true);
        list.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_add) {
            String title = edtTitle.getText().toString();
            String subTitle = edtSubTitle.getText().toString();

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(subTitle)) {
                dbHelper.addFeedEntry(title, subTitle);
            }
        } else if (v.getId() == R.id.btn_delete) {
            String title = edtTitle.getText().toString();
            if (!TextUtils.isEmpty(title)) {
                dbHelper.deleteFeedEntry(title);
            }
        } else if (v.getId() == R.id.btn_update) {
            String title = edtTitle.getText().toString();
            String subTitle = edtSubTitle.getText().toString();

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(subTitle)) {
                dbHelper.updateFeedEntry(title, subTitle);
            }
        }
        adapter.swapCursor(dbHelper.getFeedEntriesCursor());
    }

    class FeedEntryAdapter extends CursorAdapter {

        public FeedEntryAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return getLayoutInflater().inflate(android.R.layout.simple_list_item_2, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView tvTitle = (TextView) view.findViewById(android.R.id.text1);
            TextView tvSubtitle = (TextView) view.findViewById(android.R.id.text2);

            int titleColumnIndex = cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE);
            int subtitleColumnIndex = cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE);

            tvTitle.setText(cursor.getString(titleColumnIndex));
            tvSubtitle.setText(cursor.getString(subtitleColumnIndex));
        }
    }
}
