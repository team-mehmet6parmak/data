package com.mehmet6parmak.training.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by maltiparmak on 24.10.2016.
 */

public class SharedPreferencesHelper {

    public static final String KEY_OPEN_TIMES = "open-times";

    private static SharedPreferencesHelper instance;

    private static final String NAME = "data-demo";

    public static void init(Context context) {
        if (instance == null) {
            instance = new SharedPreferencesHelper(context);
        }
    }

    public static SharedPreferencesHelper getInstance() {
        return instance;
    }

    SharedPreferences prefs;

    public SharedPreferencesHelper(Context context) {
        prefs = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }


    public void set(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    public void set(String key, int value) {
        prefs.edit().putInt(key, value).apply();
    }

    public String get(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    public Integer get(String key, int defaultValue) {
        return prefs.getInt(key, defaultValue);
    }

    public boolean isFirstUse() {
        return get(KEY_OPEN_TIMES, 0) == 0;
    }

    public int getTimesTheAppOpened() {
        return get(KEY_OPEN_TIMES, 0);
    }

    public void appOpened() {
        int value = getTimesTheAppOpened();
        set(KEY_OPEN_TIMES, value + 1);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    public void remove(String key) {
        prefs.edit().remove(key).apply();
    }

    public void clear() {
        prefs.edit().clear().apply();
    }
}
