package com.mehmet6parmak.training.data;

import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class ExternalStorageActivity extends AppCompatActivity {

    private static final String TAG = "ExternalStorageActivity";

    private static final String ALBUM_NAME = "data-demo-application";

    TextView tvState;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_external_storage);

        tvState = (TextView) findViewById(R.id.tv_state);
        list = (ListView) findViewById(R.id.list);

        if (isExternalStorageWritable()) {
            tvState.setText("Available & Writable");
        } else if (isExternalStorageReadable()) {
            tvState.setText("Available & Readable but not Writable");
        } else {
            tvState.setText("Not Available");
        }
    }

    public void onPhotoListClicked(View view) {
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File appFolder = new File(file, "data-demo-application");
        if (!appFolder.exists()) {
            boolean result = appFolder.mkdirs();
        }

        String[] filenames = file.list();
        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, filenames));
    }

    public void onWhatsAppModifyClicked(View view) throws IOException {
        File externalDirectory = Environment.getExternalStorageDirectory();
        File whatsAppDirectory = new File(externalDirectory, "WhatsApp");

        if (whatsAppDirectory.exists()) {
            File whatsAppDatabasesDirectory = new File(whatsAppDirectory, "Databases");
            if (whatsAppDatabasesDirectory.exists()) {
                File file = new File(whatsAppDatabasesDirectory, "data-demo.txt");
                if (!file.exists()) {
                    boolean result = file.createNewFile();
                    if (result)
                        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, whatsAppDatabasesDirectory.list()));
                }
            } else {
                Toast.makeText(this, "whatsapp databases directory does not exists", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "whatsapp directory does not exists", Toast.LENGTH_SHORT).show();
        }
    }

    public void onWhatsAppClicked(View view) {
        File externalDirectory = Environment.getExternalStorageDirectory();
        File whatsAppDirectory = new File(externalDirectory, "WhatsApp");

        if (whatsAppDirectory.exists()) {
            File whatsAppDatabasesDirectory = new File(whatsAppDirectory, "Databases");
            if (whatsAppDatabasesDirectory.exists()) {
                String[] files = whatsAppDatabasesDirectory.list();
                list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, files));
            } else {
                Toast.makeText(this, "whatsapp databases directory does not exists", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "whatsapp directory does not exists", Toast.LENGTH_SHORT).show();
        }
    }

    public void onWhatsAppDeleteClicked(View view) {
        File externalDirectory = Environment.getExternalStorageDirectory();
        File whatsAppDirectory = new File(externalDirectory, "WhatsApp");

        if (whatsAppDirectory.exists()) {
            File whatsAppDatabasesDirectory = new File(whatsAppDirectory, "Databases");
            if (whatsAppDatabasesDirectory.exists()) {
                File file = new File(whatsAppDatabasesDirectory, "data-demo.txt");
                if (file.exists()) {
                    boolean result = file.delete();
                    if (result)
                        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, whatsAppDatabasesDirectory.list()));
                }
            } else {
                Toast.makeText(this, "whatsapp databases directory does not exists", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "whatsapp directory does not exists", Toast.LENGTH_SHORT).show();
        }
    }

    public void onPrivateExternalStorage(View view) throws IOException {
        Context context = ExternalStorageActivity.this;
        File appDirectory = context.getExternalFilesDir(null);

        int fileCount = 10;

        for (int i = 0; i < fileCount; i++) {
            String filename = String.valueOf(i) + ".txt";
            File file = new File(appDirectory, filename);

            if (!file.exists())
                file.createNewFile();
        }

        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, appDirectory.list()));
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
