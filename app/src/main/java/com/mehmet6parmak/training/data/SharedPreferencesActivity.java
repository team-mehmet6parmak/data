package com.mehmet6parmak.training.data;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SharedPreferencesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvAppOpenTimes;
    EditText edtKey;
    EditText edtValue;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        tvAppOpenTimes = (TextView) findViewById(R.id.tv_app_open_times);
        edtKey = (EditText) findViewById(R.id.edt_key);
        edtValue = (EditText) findViewById(R.id.edt_value);

        listView = (ListView) findViewById(R.id.list);

        findViewById(R.id.btn_add).setOnClickListener(SharedPreferencesActivity.this);
        findViewById(R.id.btn_clear_all).setOnClickListener(SharedPreferencesActivity.this);

        if (SharedPreferencesHelper.getInstance().isFirstUse()) {
            tvAppOpenTimes.setText("This is the first time you use our app!\nWelcome!");
            SharedPreferencesHelper.getInstance().appOpened();
        } else {
            SharedPreferencesHelper.getInstance().appOpened();
            int value = SharedPreferencesHelper.getInstance().getTimesTheAppOpened();

            tvAppOpenTimes.setText("This is the " + value + ". time you use our app!\nWelcome Again!");
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Adapter adapter = (Adapter) parent.getAdapter();
                adapter.removeItemAt(position);

            }
        });
        listView.setAdapter(new Adapter());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_add) {
            String key = edtKey.getText().toString();
            String value = edtValue.getText().toString();

            if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
                SharedPreferencesHelper.getInstance().set(key, value);

                edtValue.setText("");
                edtKey.setText("");

                updateAdapter();
            }
        } else if (v.getId() == R.id.btn_clear_all) {
            SharedPreferencesHelper.getInstance().clear();

            updateAdapter();
        }
    }

    public void updateAdapter() {
        listView.setAdapter(new Adapter());
    }


    class Adapter extends BaseAdapter {

        private List<String> keys;
        private List<String> values;

        public Adapter() {

            Map<String, ?> preferences = SharedPreferencesHelper.getInstance().getPrefs().getAll();

            keys = new ArrayList<>(preferences.keySet());
            values = new ArrayList<>();

            for (String key : keys) {
                values.add(preferences.get(key).toString());
            }
        }

        @Override
        public int getCount() {
            return keys.size();
        }

        @Override
        public Object getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null)
                view = LayoutInflater.from(SharedPreferencesActivity.this).inflate(android.R.layout.simple_list_item_2, parent, false);
            else
                view = convertView;

            TextView tvTitle = (TextView) view.findViewById(android.R.id.text1);
            TextView tvValue = (TextView) view.findViewById(android.R.id.text2);

            tvTitle.setText(keys.get(position));
            tvValue.setText(values.get(position));

            return view;
        }

        public void removeItemAt(int position) {
            String key = keys.get(position);
            SharedPreferencesHelper.getInstance().remove(key);

            updateAdapter();
        }
    }


}
