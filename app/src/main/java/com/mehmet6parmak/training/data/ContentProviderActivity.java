package com.mehmet6parmak.training.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class ContentProviderActivity extends AppCompatActivity {

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_provider);

        list = (ListView) findViewById(R.id.list);

        final String[] PROJECTION = new String [] {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME
        };

        Uri URI = ContactsContract.Contacts.CONTENT_URI;
        Cursor cursor = getContentResolver().query(URI, PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME);
        list.setAdapter(new CursorAdapterImpl(this, cursor, true));
    }


    public class CursorAdapterImpl extends CursorAdapter {
        public CursorAdapterImpl(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(ContentProviderActivity.this);
            return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView txt = (TextView) view.findViewById(android.R.id.text1);
            txt.setText(cursor.getString(1));
        }
    }
}
