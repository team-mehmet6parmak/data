package com.mehmet6parmak.training.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class InternetActivity extends AppCompatActivity {

    private static final String TAG = "InternetActivity";

    TextView tvStatus;
    TextView tvGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);

        tvStatus = (TextView) findViewById(R.id.tv_status);
        tvGoogle = (TextView) findViewById(R.id.tv_google);

        updateConnectionStatusLabel();
    }

    private void updateConnectionStatusLabel() {
        tvStatus.setText(isConnected() ? "Connected" : "Not Connected");
    }

    public boolean isConnected() {
        Context context = InternetActivity.this;
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void downloadGoogle(View view) {
        /**
         * making network calls on UI thread is not allowed in Android. You have to make the call on a background thread.
         */
        new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                tvGoogle.setText("Loading url...");
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    return downloadUrl(params[0]);
                } catch (IOException e) {
                    Log.d(TAG, "error downloading: " + params[0], e);
                }
                return "";
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                tvGoogle.setText(s);
            }
        }.execute("https://google.com");
    }


    public String postData(String urlString, String data) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

        writer.write(data);
        writer.flush();

        String result = readIt(conn.getInputStream());

        writer.close();

        return result;
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    public String downloadUrl(String urlToFetch) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(urlToFetch);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // Starts the query
            conn.connect();

            //http status code.
            int response = conn.getResponseCode();
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream) throws IOException {
        Reader reader;
        StringBuilder builder = new StringBuilder();

        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[2048];

        int read;
        do {
            read = reader.read(buffer);
            if (read > 0)
                builder.append(buffer, 0, read);

        } while (read != -1);

        reader.close();

        return builder.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();

        AppCompatActivity activity = this;

        IntentFilter filter = new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
        activity.registerReceiver(connectionChangeListener, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(connectionChangeListener);
    }

    ConnectivityListener connectionChangeListener = new ConnectivityListener();

    class ConnectivityListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateConnectionStatusLabel();
        }
    }
}
