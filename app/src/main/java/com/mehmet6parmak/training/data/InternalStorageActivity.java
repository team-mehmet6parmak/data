package com.mehmet6parmak.training.data;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InternalStorageActivity extends AppCompatActivity {

    EditText edtFileInput;
    EditText edtFilename;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_storage);

        edtFileInput = (EditText) findViewById(R.id.edt_file_input);
        edtFilename = (EditText) findViewById(R.id.edt_filename);
        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) parent.getAdapter();
                String filename = adapter.getItem(position);

                /* Long version */
                File internalStorage = InternalStorageActivity.this.getFilesDir();
                File file = new File(internalStorage, filename);

                file.delete();

                // short version, this method is a member of Context object.
                //deleteFile(filename);

                updateFileList();
            }
        });

        updateFileList();
    }

    private void updateFileList() {
        String[] files = fileList();
        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, files));
    }

    public void onReadFromAssets(View view) throws IOException {
        String content = readFromAssets("assets_file.txt");
        edtFileInput.setText(content);
    }

    public void onReadFromRawResources(View view) throws IOException {
        String content = readFromRawResources();
        edtFileInput.setText(content);
    }

    public void onReadFromInternalStorage(View view) throws IOException {
        String content = readFromInternalStorage(edtFilename.getText().toString());
        edtFileInput.setText(content);
    }

    public void onWriteToInternalStorage(View view) throws IOException {
        writeToInternalStorage(edtFilename.getText().toString(), edtFileInput.getText().toString());
    }

    public String readFromAssets(String filename) throws IOException {
        Context context = InternalStorageActivity.this;
        AssetManager assetManager = context.getAssets();

        InputStream inputStream = assetManager.open(filename);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

        String line = reader.readLine();
        reader.close();

        return line;
    }

    public String readFromRawResources() throws IOException {
        Context context = InternalStorageActivity.this;
        Resources res = context.getResources();

        InputStream inputStream = res.openRawResource(R.raw.raw_file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

        String line = reader.readLine();
        reader.close();

        return line;
    }

    public String readFromInternalStorage(String filename) throws IOException {
        if (!TextUtils.isEmpty(filename)) {
            Context context = InternalStorageActivity.this;
            File internalFilesDirectory = context.getFilesDir();

            File file = new File(internalFilesDirectory, filename);

            if (file.exists()) {

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                FileInputStream fileInputStream = new FileInputStream(file);

                int read;
                while ((read = fileInputStream.read(buffer, 0, buffer.length)) == 4096) {
                    output.write(buffer);
                }
                output.write(buffer, 0, read);

                fileInputStream.close();

                byte[] allFileContent = output.toByteArray();
                String utf8String = new String(allFileContent, "UTF-8");

                return utf8String;

            } else {
                Toast.makeText(this, filename + " does not exist", Toast.LENGTH_SHORT).show();
            }
        }
        return "";
    }

    public void writeToInternalStorage(String filename, String content) throws IOException {
        if (!TextUtils.isEmpty(filename)) {
            Context context = InternalStorageActivity.this;
            File internalFilesDirectory = context.getFilesDir();

            File file = new File(internalFilesDirectory, filename);
            if (file.exists() || file.createNewFile()) {
                FileOutputStream outputStream = new FileOutputStream(file);
                byte[] contentArray = content.getBytes("UTF-8");
                outputStream.write(contentArray);
                outputStream.close();

                updateFileList();
            }
        }
    }
}
