package com.mehmet6parmak.training.data;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferencesHelper.init(getApplicationContext());
    }

    public void onSharedPreferencesClicked(View view) {
        startActivity(new Intent(this, SharedPreferencesActivity.class));
    }

    public void onFileStorageClicked(View view) {
        startActivity(new Intent(this, InternalStorageActivity.class));
    }

    public void onExternalStorageClicked(View view) {
        startActivity(new Intent(this, ExternalStorageActivity.class));
    }

    public void onDatabaseClicked(View view) {
        startActivity(new Intent(this, DatabaseActivity.class));
    }

    public void onContentProviderClicked(View view) {
        startActivity(new Intent(this, ContentProviderActivity.class));
    }

    public void onInternetClicked(View view){
        startActivity(new Intent(this, InternetActivity.class));
    }
}
